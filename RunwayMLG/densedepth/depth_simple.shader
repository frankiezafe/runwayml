shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform sampler2D texture_metallic : hint_white;
uniform vec4 metallic_texture_channel;
uniform sampler2D texture_roughness : hint_white;
uniform vec4 roughness_texture_channel;
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;

uniform sampler2D texture_depth : hint_black;
uniform float depth;
uniform float depth_power;
uniform mat3 kernel;
uniform float depth_smooth;
uniform float normal_smooth;

float get_depth( sampler2D tex, vec2 uv ) {
	mat3 dk = mat3(
		vec3(texture( tex, uv + vec2( -depth_smooth, -depth_smooth ) ).r, texture( tex, uv + vec2( 0.0, -depth_smooth ) ).r, texture( tex, uv + vec2( depth_smooth, -depth_smooth ) ).r),
		vec3(texture( tex, uv + vec2( -depth_smooth, 0.0 ) ).r, texture( tex, uv + vec2( 0.0, 0.0 ) ).r, texture( tex, uv + vec2( depth_smooth, 0.0 ) ).r),
		vec3(texture( tex, uv + vec2( -depth_smooth, depth_smooth ) ).r, texture( tex, uv + vec2( 0.0, depth_smooth ) ).r, texture( tex, uv + vec2( depth_smooth, depth_smooth ) ).r)
		);
	dk *= kernel;
	float d = dk[0][0]+dk[0][1]+dk[0][2]+dk[1][0]+dk[1][1]+dk[1][2]+dk[2][0]+dk[2][1]+dk[2][2];
	d /= kernel[0][0]+kernel[0][1]+kernel[0][2]+kernel[1][0]+kernel[1][1]+kernel[1][2]+kernel[2][0]+kernel[2][1]+kernel[2][2];
//	float dist = pow( abs( 0.5 - d ), depth_power );
//	if ( d < 0.5 ) {
//		dist *= -1.0;
//	}
	return ( 1.0 - d ) * depth;
}

void vertex() {
	UV = UV * uv1_scale.xy + uv1_offset.xy;
	VERTEX.y = get_depth( texture_depth, UV );
	vec2 e = vec2(normal_smooth, 0.0);
	vec3 normal = normalize(
		vec3(
			get_depth( texture_depth, UV - e ) - get_depth( texture_depth, UV + e ), 
			2.0 * e.x, 
			get_depth( texture_depth, UV - e.yx ) - get_depth( texture_depth, UV + e.yx )
			));
	NORMAL = normal;
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	ALBEDO = albedo.rgb * albedo_tex.rgb;
	float metallic_tex = dot(texture(texture_metallic,base_uv),metallic_texture_channel);
	METALLIC = metallic_tex * metallic;
	float roughness_tex = dot(texture(texture_roughness,base_uv),roughness_texture_channel);
	ROUGHNESS = roughness_tex * roughness;
	SPECULAR = specular;
}
